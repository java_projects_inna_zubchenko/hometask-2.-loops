package com.epam.test.automation.java.practice2;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MainTest {

    @DataProvider(name = "testTask1")
            public Object[][] dataProviderMethod1(){
            return new Object[][] { { 1217, 9}, { 222, 0 }, {231, 4} };
            }

    @Test(dataProvider = "testTask1")
    public void task1Test(int value, int expected){
        var actual = Main.task1(value);
        Assert.assertEquals( actual, expected,"The method task1 returns incorrect value");
    }

    @Test()
        public void exceptionTask1Test(){
        try {
            Main.task1( -321);
            Assert.fail("IllegalArgumentException expected");
        } catch(IllegalArgumentException expected){

        }
    }

    @DataProvider(name = "testTask2")
    public Object[][] dataProviderMethod2(){
        return new Object[][] { { 15, 4}, { 128, 1 }, {5, 2} , {1, 1}};
    }

    @Test(dataProvider = "testTask2")
    public void task2Test(int value, int expected){
        var actual = Main.task2(value);
        Assert.assertEquals( actual, expected, "The method task2 returns incorrect value");
    }
    @Test()
    public void exceptionTask2Test(){
        try {
            Main.task2( -15);
            Assert.fail("IllegalArgumentException expected");
        } catch(IllegalArgumentException expected){

        }
    }
    @DataProvider(name = "testTask3")
    public Object[][] dataProviderMethod3(){
        return new Object[][] { { 7, 20}, { 8, 33 }, {1, 0} , {10, 88}};
    }

    @Test(dataProvider = "testTask3")
    public void task3Test(int value, int expected){
        var actual = Main.task3(value);
        Assert.assertEquals(actual, expected, "The method task3 returns incorrect value");}

    @Test()
    public void exceptionTask3Test(){
        try {
            Main.task3( -2);
            Assert.fail("IllegalArgumentException expected");
        } catch(IllegalArgumentException expected){

        }
    }
}
