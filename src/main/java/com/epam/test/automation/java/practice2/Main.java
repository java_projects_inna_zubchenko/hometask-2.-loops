package com.epam.test.automation.java.practice2;

public class Main {

    private Main(){

    }
    public static int task1(int value) {
        if(value<=0) {
            throw new IllegalArgumentException();
        }
        int sum = 0;
        do
        {
            if (value % 2 == 1)
            {
                sum += (value % 10);
            }
            value /= 10;
        }
        while (value > 0);
        return sum;
    }

    public static int task2(int value) {
        if(value<=0) {
            throw new IllegalArgumentException();
        }
        int sumUnits = 0;
        do
        {
            sumUnits += (value % 2);
            value /= 2;
        } while (value > 0);
        return sumUnits;
    }

    public static int task3(int value) {
        if(value<=0) {
            throw new IllegalArgumentException();
        }
            int firstNumber = 0;
            int secondNumber = 1;
            int sumNumbers = 0;
            for (int i = 1; i < value; i++) {
                int nextNumber = firstNumber + secondNumber;
                firstNumber = secondNumber;
                secondNumber = nextNumber;
                sumNumbers += firstNumber;
            }
            return sumNumbers;
    }
}
